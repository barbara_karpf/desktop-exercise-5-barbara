﻿using Desktop_Exercise_5.Interfaces;
using System;
using Desktop_Exercise_5.Utility;

namespace Desktop_Exercise_5.Models
{
  // order should be private member variables, Constructor, public properties
  public class Sport : ISport
  {
    #region Private Properties
    private string _sportName;
    private Lookups.SportCategory _sportCategory;
    #endregion

    #region Constructor
    public Sport(int noPlayers, Lookups.SportCategory sportCategoryIn, string sportNameIn)
    {
      NumberOfPlayers = noPlayers;
      _sportCategory = sportCategoryIn;
      _sportName = sportNameIn;

   }
    #endregion

    #region Public Properties

    // public property that CAN BE set with Constructor
    public int NumberOfPlayers { get; set; }

    // public property that ARE NOT set with Constructor
    public decimal Length { get; set; }
    public decimal Width { get; set; }

    // public read only propery to access private member of class
    public Lookups.SportCategory SportCategory
    {
      get { return _sportCategory; }
    }

    // public read only propery to access private member of class
      public string SportName
    {
      get { return _sportName; }
    }

    #endregion

    #region Public Methods
    public void WriteSportProperties()
    {
      Console.WriteLine("***** Sport Properties *****");
      Console.WriteLine("Name: {0}", _sportName);
      Console.WriteLine("Category: {0}", _sportCategory);
      Console.WriteLine("# of Players: {0}", NumberOfPlayers);
      Console.WriteLine("Court Dimenstions: {0} x {1}\r\n", Length, Width);
    }
    #endregion

    #region Private Methods
    // private methods would go here
    #endregion
  }
}
