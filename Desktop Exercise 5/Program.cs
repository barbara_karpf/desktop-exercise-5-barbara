﻿using System;
using Desktop_Exercise_5.Models;
using Desktop_Exercise_5.Utility;
using System.Collections.Generic;

namespace Desktop_Exercise_5
{
  class Program
  {
    static void Main(string[] args)
    {
      List<Sport> sportList = new List<Sport>();

      Sport volleyBall = new Sport(6, Lookups.SportCategory.Team, "Volleyball");
      volleyBall.Length = 60;
      volleyBall.Width = 30;

      Sport tennis = new Sport(1, Lookups.SportCategory.Single, "Tennis");
      tennis.Length = 78;
      tennis.Width = 36;

      sportList.Add(volleyBall);
      sportList.Add(tennis);

      foreach (var item in sportList) {
        item.WriteSportProperties();
      }

      // use to keep console window open
      Console.ReadKey();
    }
  }
}
